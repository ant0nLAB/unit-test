# NodeJS Project with Express, Mocha, and Chai

This repository contains a NodeJS project implementing a basic calculator functionality using Express for API endpoints, Mocha for testing, and Chai for assertions.